import React from "react";

const Hello = (props: React.HTMLProps<HTMLHeadingElement>) => {
  return <h1 {...props}>{props.children || "Hello world"}</h1>;
};

export { Hello };
